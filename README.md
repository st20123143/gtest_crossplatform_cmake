# README #

Updated example for Multi-player Games Development (CIS6014) the original was for a much older set of notes on CMake with separate Linux and Windows versions. The structure is a little complex but reflects the later projects where the networking library is separted from gtest and its supporting libraries. 

## Usage ##

1. Clone the repository

2. Navigate to the repository and create a build folder, note this should be at the top level of the repostitory. 

3. Run CMake and generate the required project files (for your choice of OS/IDE). 

4. Two targets (projects in some IDEs) are produced, one is a mock application the other a test harness. 
