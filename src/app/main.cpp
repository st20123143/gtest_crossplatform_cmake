#include "NameList.h"

#include <iostream>

#if _WIN32
#include <Windows.h>
#endif

#if _WIN32
int WINAPI WinMain( _In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPSTR lpCmdLine, _In_ int nCmdShow )
{
	UNREFERENCED_PARAMETER( hPrevInstance );
	UNREFERENCED_PARAMETER( lpCmdLine );
    
    std::cout << "Fake main, for runtime" << std::endl;

}
#else

int main(int argc, const char** argv)
{
	std::cout << "Fake main, for runtime" << std::endl;
}
#endif